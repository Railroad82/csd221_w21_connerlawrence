/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab1.Q6;

import java.util.*;

/**
 *
 * @author Conner Lawrence
 */
public class Person {
    
    private String firstName;
    private String lastName;
    private int sinNumber;
    

   /*public static void addPerson(){

    Scanner addPerson = new Scanner(System.in);  
    System.out.println("First Name: ");
    String firstName = addPerson.next();
    System.out.println("Last Name: ");
    String lastName = addPerson.next();
    System.out.println("SIN Number: ");
    String sinNumber = addPerson.next();

    System.out.println("You've added " + firstName + " " + lastName + ". SIN: " + sinNumber);
    
   
   }*/
   /*public static void listPeople(){
       ArrayList<String> People = new ArrayList<String>();
       
       People.add(addPerson.firstName);
   }*/

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the sinNumber
     */
    public int getSinNumber() {
        return sinNumber;
    }

    /**
     * @param sinNumber the sinNumber to set
     */
    public void setSinNumber(int sinNumber) {
        this.sinNumber = sinNumber;
    }
}
     

   