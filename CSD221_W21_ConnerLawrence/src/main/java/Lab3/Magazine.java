/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

/**
 *
 * @author Conner Lawrence
 */
public class Magazine extends Publication {
    public int orderQty;
    public String curIssue;

    public Magazine() {
        orderQty=10;
        curIssue="now";
    }

    public Magazine(int orderQty, String curIssue) {
        this.orderQty = orderQty;
        this.curIssue = curIssue;
    }

    public void adjustQty(int qty){
        orderQty+=qty;
    }
    
    public void receiveNewIssue(String date){
        curIssue=date;
    }

    @Override
    public String toString() {
        return super.toString()+" Current Issue: "+curIssue+" Order Qty: "+orderQty; 
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); 
    }

    
    
    public int getOrderQty() {
        return orderQty;
    }

    
    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    
    public String getCurIssue() {
        return curIssue;
    }

    
    public void setCurIssue(String curIssue) {
        this.curIssue = curIssue;
    }
    
    
}

