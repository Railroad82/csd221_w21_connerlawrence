/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

/**
 *
 * @author Conner Lawrence
 */
public class Publication {
    public String author;
    public String title;
    public double price;
    public int copies;
    
    public Publication(){
        title="no title";
    }

    public void sellCopy(){
        
    }

    @Override
    public String toString() {
        return "Title: "+title+", Price: "+price+", Copies: "+copies;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    
    
    
    public String getTitle() {
        return title;
    }

    
    public void setTitle(String title) {
        this.title = title;
    }

    
    public double getPrice() {
        return price;
    }

    
    public void setPrice(double price) {
        this.price = price;
    }

    
    public int getCopies() {
        return copies;
    }

    
    public void setCopies(int copies) {
        this.copies = copies;
    }
    
}

