/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

/**
 *
 * @author conner
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.sql.Date; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;
import lab5.entities.Books; 
import lab5.entities.DiscMag;
import lab5.entities.Magazine;
import lab5.entities.Pencil;
import lab5.entities.Publication;
import lab5.entities.Ticket;
import lab5.controllers.BooksJpaController;
import lab5.controllers.DiscMagJpaController;
import lab5.controllers.MagazineJpaController;
import lab5.controllers.PencilJpaController;
import lab5.controllers.PublicationJpaController;
import lab5.controllers.TicketJpaController;


public class App {
    private List<Books> bookList=new ArrayList<>();
    private List<Magazine> magList=new ArrayList<>();
    private List<Publication> publicationList=new ArrayList<>();
    
    
    
    public void run() {

        EntityManagerFactory emf = null;
        EntityManager em = null;
        
        Scanner input = new Scanner(System.in);
        int choice = 0;
        int choice2 = 0;       
        int answer = 0;
        int counter = 0;
        boolean done = false;

        while (!done) {
            try {

                emf = Persistence.createEntityManagerFactory("lab5_PU");            
                em = emf.createEntityManager();
                Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager Created");
                   

                BooksJpaController booksCtrl = new BooksJpaController(emf);            
                DiscMagJpaController discmagCtrl = new DiscMagJpaController(emf);
                MagazineJpaController magazineCtrl = new MagazineJpaController(emf);
                PencilJpaController pencilCtrl = new PencilJpaController(emf);
                TicketJpaController ticketCtrl = new TicketJpaController(emf);
                PublicationJpaController pubCtrl = new PublicationJpaController(emf);
                
                List<Books> books = booksCtrl.findBooksEntities();
                
                
                em.getTransaction().begin();
                Pencil p = new Pencil();
                                    
                p.setName("HB2");                                                                      
                p.setPrice(1.99);
                p.setCopies(100000);
                                    
                pencilCtrl.create(p);
                em.getTransaction().commit();

                    System.out.println("-------------------------Main---------------------------------");
                    System.out.println("1. Books");
                    System.out.println("2. Tickets");
                    System.out.println("3. Magazines");
                    System.out.println("4. Pencils");
                    System.out.println("9. Quit");
                    System.out.println("--------------------------------------------------------------");
                    choice = input.nextInt();

                    switch (choice) {
                        case 1:
                            //choice = 1;
                            //
                            System.out.println("-------------------------Books--------------------------------");
                            System.out.println("1. Add a Book");
                            System.out.println("2. Edit a Book");
                            System.out.println("3. Delete a Book");
                            System.out.println("4. Sell a Book");
                            System.out.println("5. Quit");
                            System.out.println("--------------------------------------------------------------");
                            choice2 = input.nextInt();
                            
                            switch (choice2){
                                case 1:
                                    //choice2 = 1;
                                    
                                    em.getTransaction().begin();
                                    Books book1 = new Books();
                                    input = new Scanner(System.in);
                                    System.out.println("Enter Title: ");
                                    book1.setTitle(input.nextLine());

                                    input = new Scanner(System.in);
                                    System.out.println("Quantity to Order: ");
                                    book1.setOrderQTY(input.nextInt());
                                    
                                    input = new Scanner(System.in);
                                    System.out.println("Enter Author: ");
                                    book1.setAuthor(input.nextLine());

                                    input = new Scanner(System.in);
                                    System.out.println("Enter Price: ");
                                    book1.setPrice(input.nextDouble());
                                    
                                    
                                    booksCtrl.create(book1);
                                    bookList.add(book1);
                                    
                                    em.getTransaction().commit();
                                    break;

                                case 2:
                                    //choice2 = 2;
                                    
                                    
                                    em.getTransaction().begin();
                                    System.out.println("Edit Book");
                                    books = booksCtrl.findBooksEntities();
                                    counter = 0; 
                                    for (Books b : books) {
                                        System.out.println(counter + " " + b.getTitle());
                                        counter++;
                                    }
                                    
                                    System.out.println("Which one to edit?");
                                    input = new Scanner(System.in);
                                    int userInput = input.nextInt();
                                    
                                    System.out.println("Enter Title: ");
                                    input = new Scanner(System.in);
                                    String title = input.nextLine();
                                    books.get(userInput - 1).setTitle(title);
                                    
                                    System.out.println("Enter Order Qunatiy: ");
                                    input = new Scanner(System.in);
                                    int order = input.nextInt();
                                    books.get(userInput - 1).setOrderQTY(order);
                                    
                                    System.out.println("Enter Author: ");
                                    input = new Scanner(System.in);
                                    String author = input.nextLine();
                                    books.get(userInput - 1).setAuthor(author);
                                    
                                    System.out.println("Enter Price: ");
                                    input = new Scanner(System.in);
                                    Double price = input.nextDouble();
                                    books.get(userInput - 1).setPrice(price);
                                    
                                    booksCtrl.edit(books.get(userInput - 1));
                                    em.getTransaction().commit();
                                    books.set(userInput - 1, books.get(userInput - 1));
                                    
                                    /*List<Books> books = booksCtrl.findBooksEntities();
                                    for (Books b : books) {
                                    System.out.println(b.getTitle());
                                    }
                                    for (Books b : books) {
                                    b.setTitle(book1.getTitle() + "_" + System.currentTimeMillis());
                                    booksCtrl.edit(b);
                                    }
                                    books = booksCtrl.findBooksEntities();
                                    for (Books b : books) {
                                        System.out.println(b.getTitle());
                                    }*/
                                    
                                    break;
                                case 3:
                                    //choice2 = 3;
                                            
                                    em.getTransaction().begin();
                                    System.out.println("Which book do you want to delete?");
                                    input = new Scanner(System.in);
                                    userInput = input.nextInt();
                                    books = booksCtrl.findBooksEntities();
                                    for (Books b : books) {
                                        System.out.println(b.getTitle());
                                    }
                                    
                                    booksCtrl.destroy(books.get(userInput - 1).getId());
                                    //put system out
                                    
                                    em.getTransaction().commit();
                                    break;
                                    
                                case 4: 
                                    //choice2 = 4;
                                    
                                    em.getTransaction().begin();
                                    System.out.println("Sell Book");
                                    books = booksCtrl.findBooksEntities();
                                    counter = 0; 
                                    for (Books b : books) {
                                        System.out.println(counter + " " + b.getTitle());
                                        counter++;
                                    }
                                    
                                    System.out.println("Which one to sell?");
                                    input = new Scanner(System.in);
                                    userInput = input.nextInt();
                                    
                                    int userCopies = 0;
                                    System.out.println("How many copies do you want to sell?");
                                    input = new Scanner (System.in);
                                    userCopies = input.nextInt();
                                    
                                    Books b = new Books();
                                    
                                    b.setCopies(b.getCopies() - userCopies);
                                    

                                    if (b.getCopies() == 0)
                                    {
                                        
                                        booksCtrl.destroy(b.getId());
                                        
                                    }else{
                                        System.out.println("You sold " + b.getCopies() + "copy/copies of " + b.getTitle() + " for $" + b.getPrice() );
                                    }
                                    em.getTransaction().commit();
                                    break;
                                    
                                case 5:
                                    //choice2 = 5;
                            
                                    System.out.println("Go back to menu?");
                                    System.out.println("1. Yes");
                                    System.out.println("2. No (This will close the program)");
                                    answer = input.nextInt();

                                    if (answer == 1){
                                        break;
                                    }
                                    if (answer == 2){
                                        done = true;
                                        break;
                                    }
                            }
                            break;

                        case 2:
                            //choice = 2;
                            
                            System.out.println("-------------------------Tickets------------------------------");
                            System.out.println("1. Sell a Ticket");
                            System.out.println("2. Exit");
                            System.out.println("--------------------------------------------------------------");
                            choice2 = input.nextInt();
                                                        
                            switch(choice2){
                                case 1:
                                   //choice2 = 1;
                                    
                                    em.getTransaction().begin();
                                    Ticket t = new Ticket();
                                    input = new Scanner(System.in);
                                    System.out.println("Enter Name: ");
                                    t.setName(input.nextLine());
                                    
                                    input = new Scanner(System.in);
                                    System.out.println("Enter Copies: ");
                                    t.setCopies(input.nextInt());
                                    

                                    input = new Scanner(System.in);
                                    System.out.println("Enter Price: ");
                                    t.setPrice(input.nextDouble());
                                    
                                    ticketCtrl.create(t);
                                    em.getTransaction().commit();
                                    
                                    System.out.println("You sold " + t.getCopies() + "ticket(s) of " + t.getName() + " for $" + t.getPrice() );
                                    break;
                                    
                                case 2:
                                    //choice2 = 2;
                                    
                                    System.out.println("Go back to menu?");
                                    System.out.println("1. Yes");
                                    System.out.println("2. No (This will close the program)");
                                    answer = input.nextInt();

                                    if (answer == 1){
                                        break;
                                    }
                                    if (answer == 2){
                                        done = true;
                                        break;
                                    }                                    
                            }
                            break;
                                                    
                        case 3:
                            //choice = 3;
                            
                            System.out.println("-------------------------Magazine--------------------------------");
                            System.out.println("1. Add a Magazine");
                            System.out.println("2. Edit a Magazine");
                            System.out.println("3. Delete a Magazine");
                            System.out.println("4. Sell a Magazine");
                            System.out.println("5. Quit");
                            System.out.println("--------------------------------------------------------------");
                            choice2 = input.nextInt();
                            
                            switch (choice2){
                                case 1:
                                    //choice2 = 1;
                                    
                                    em.getTransaction().begin();
                                    Magazine mag1 = new Magazine();
                                    input = new Scanner(System.in);
                                    System.out.println("Enter Title: ");
                                    mag1.setTitle(input.nextLine());

                                    input = new Scanner(System.in);
                                    System.out.println("Quantity to Order: ");
                                    mag1.setOrderQTY(input.nextInt());
                                    
                                    
                                    long currDate = System.currentTimeMillis();
                                    Date sqlDate = new Date(currDate);
                                    
                                    mag1.setCurrIssue(sqlDate);

                                    input = new Scanner(System.in);
                                    System.out.println("Enter Price: ");
                                    mag1.setPrice(input.nextDouble());
                                    
                                    magazineCtrl.create(mag1);
                                    em.getTransaction().commit();
                                    break;
                            //
                            //listBooks();
                                case 2:
                                    //choice2 = 2;
                                     em.getTransaction().begin();
                                    System.out.println("Edit Magazine");
                                    magList = magazineCtrl.findMagazineEntities();
                                    counter = 0; 
                                    for (Magazine m : magList) {
                                        System.out.println(counter + " " + m.getTitle());
                                        counter++;
                                    }
                                    
                                    System.out.println("Which one to edit?");
                                    input = new Scanner(System.in);
                                    int userInput = input.nextInt();
                                    
                                     em.getTransaction().begin();
                                    mag1 = new Magazine();
                                    input = new Scanner(System.in);
                                    System.out.println("Enter Title: ");
                                    mag1.setTitle(input.nextLine());

                                    input = new Scanner(System.in);
                                    System.out.println("Quantity to Order: ");
                                    mag1.setOrderQTY(input.nextInt());
                                    
                                    
                                    currDate = System.currentTimeMillis();
                                    sqlDate = new Date(currDate);
                                    
                                    mag1.setCurrIssue(sqlDate);

                                    input = new Scanner(System.in);
                                    System.out.println("Enter Price: ");
                                    mag1.setPrice(input.nextDouble());
                                    
                                    magazineCtrl.edit(magList.get(userInput - 1));
                                    em.getTransaction().commit();
                                    magList.set(userInput - 1, magList.get(userInput - 1));
                                    
                                    break;
                                case 3:
                                    //choice2 = 3;
                                            
                                    em.getTransaction().begin();
                                    System.out.println("Delete Magazine");
                                    input = new Scanner(System.in);
                                    magList = magazineCtrl.findMagazineEntities();
                                    for (Magazine m : magList) {
                                        System.out.println(m.getTitle());
                                    }
                                    break;
                                    
                                case 4: 
                                    //choice2 = 4;
                                    em.getTransaction().begin();
                                    System.out.println("Which Magazine do you want to delete?");
                                    input = new Scanner(System.in);
                                    userInput = input.nextInt();
                                    magList = magazineCtrl.findMagazineEntities();
                                    for (Magazine m : magList) {
                                        System.out.println(m.getTitle());
                                    }
                                    
                                    magazineCtrl.destroy(magList.get(userInput - 1).getId());
                                    //put system out
                                    
                                    em.getTransaction().commit();
                                    break;
                                    
                                case 5:
                                    //choice2 = 5;
                            
                                    System.out.println("Go back to menu?");
                                    System.out.println("1. Yes");
                                    System.out.println("2. No (This will close the program)");
                                    answer = input.nextInt();

                                    if (answer == 1){
                                        break;
                                    }
                                    if (answer == 2){
                                        done = true;
                                        break;
                                    }
                            }
                            break; 

                        case 4:
                            //choice = 4;
                                                        
                            System.out.println("-------------------------Pencils------------------------------");
                            System.out.println("1. Sell a Pencil");
                            System.out.println("2. Exit");
                            System.out.println("--------------------------------------------------------------");
                            choice2 = input.nextInt();
                                                        
                            switch(choice2){
                                case 1:
                                    //choice2 = 1;
                                    //transaction -  set them
                                    int userCopies = 0;
                                    System.out.println("How many pencils do you want to sell?");
                                    input = new Scanner (System.in);
                                    userCopies = input.nextInt();
                                    
                                    p.setCopies(p.getCopies() - userCopies);
                                    
                                    
                                    if (p.getCopies() == 0)
                                    {
                                        
                                        pencilCtrl.destroy(p.getId());
                                        break;
                                        
                                    }else    
                                    {
                                    System.out.println("You sold " + p.getCopies() + " penicl(s) of " + p.getName() + " for $" + p.getPrice() );
                                    break;
                                    }
                                case 2:
                                    //choice2 = 2;
                                    
                                    System.out.println("Go back to menu?");
                                    System.out.println("1. Yes");
                                    System.out.println("2. No (This will close the program)");
                                    answer = input.nextInt();

                                    if (answer == 1){
                                        break;
                                    }
                                    if (answer == 2){
                                        done = true;
                                        break;
                                    }                                    
                            }
                            break;
                            
                        case 9:
                            //choice = 9;
                            done = true;
                            break;
                        default:
                            System.out.println("Wrong entry try again.");
                    }
            }  
            catch(Exception e){
                System.out.println(e.getMessage());
        }
        finally {
            if(emf != null) {
                emf.close();
            }
        }
    }
    
}}

