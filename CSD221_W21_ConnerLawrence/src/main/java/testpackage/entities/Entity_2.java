package testpackage.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author conne
 */
@Entity
public class Entity_2 extends Publication {

    @Basic
    private String attribute;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

}