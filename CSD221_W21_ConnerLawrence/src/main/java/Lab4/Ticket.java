/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

/**
 *
 * @author Conner Lawrence
 */
public class Ticket implements SaleableItem{
    private double price;
    @Override
    public void sellCopy() {
        System.out.println("Print out ticket");
    }

    @Override
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }
    
}
