/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import Lab3.*;


/**
 *
 * @author Conner Lawrence
 */
public class Book extends Publication{
    private String author;
    
    
    @Override
    public void sellCopy() {
        System.out.println("Selling Book");
    }

    public Book() {
        author="no author";
    }

    public Book(String author) {
        this.author = author;
    }
    public Book(String author, String title, int copies, double price) {
        this.author = author;
        this.title = title;
        this.copies = copies;
        this.price = price;
    }
    
    
    public String getAuthor() {
        return author;
    }
    
    public void orderCopies(int copies){
        setCopies(copies);
    }
    

    @Override
    public String toString() {
        return super.toString()+", Author: "+author; 
    }
    
    

    
    public void setAuthor(String author) {
        this.author = author;
    }
    
    
    
}

