/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import Lab3.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Conner Lawrence
 */

public class App {
    private List<Book> bookList=new ArrayList<>();
    private List<Magazine> magList=new ArrayList<>();
    private List<DiscMag> dMagList=new ArrayList<>();
    List<SaleableItem> items=new ArrayList<>();
    CashTill till=new CashTill();
    
    private List<Publication> publicationList=new ArrayList<>();
    private static Scanner input = new Scanner(System.in);
    
    public void run(){
        Book book=new Book();
        Book book2=new Book("Harper Lee","To Kill a Mocking Bird", 10, 15.99);
        Book book3=new Book("Dan Brown", "Da Vinci Code", 20, 25.99);
        
        Magazine mag=new Magazine(13, "14-April-2021");
        
        DiscMag dMag=new DiscMag("London Records","Charlie Grace",25);
        
        
        
        bookList.add(book);
        bookList.add(book2);
        bookList.add(book3);
        
        magList.add(mag);
        
        dMagList.add(dMag);
        
        publicationList.add(book);
        publicationList.add(book2);
        publicationList.add(book3);
        publicationList.add(mag);
        publicationList.add(dMag);
        
        int n=0;
        for(Book b:bookList){
            n++;
            System.out.println(n+". book="+b);
        }
        n=0;
        for(Magazine m:magList){
            n++;
            System.out.println(n+". mag="+m);
        }
        
        n=0;
        for(Publication p:publicationList){
            n++;
            System.out.println(n+". publication="+p);
            if(p instanceof Book)
                System.out.println("p is a book"+p);
        }
        
      int choice = 0;
      int answer = 0;
        boolean done = false;

        while (!done) {
            try {
                System.out.println("-------------------------Main---------------------------------");
                System.out.println("1. Books");
                System.out.println("2. Tickets");
                System.out.println("3. Magazines");
                System.out.println("--------------------------------------------------------------");
                System.out.println("-------------------------Books--------------------------------");
                System.out.println("2. Add a Book");
                System.out.println("3. Edit a Book");
                System.out.println("4. Delete a Book");
                System.out.println("9. Quit");
                choice = input.nextInt();

                switch (choice) {
                    case 1:
                        choice = 1;
                        listBooks();
                        System.out.println("Go back to menu?");
                        System.out.println("1. Yes");
                        System.out.println("2. No (This will close the program)");
                        answer = input.nextInt();
                        
                        if (answer == 1){
                            break;
                        }
                        if (answer == 2){
                            done = true;
                            break;
                        }
                    case 2:
                        choice = 2;
                        System.out.println("-------------------------Tickets------------------------------");
                        System.out.println("1. Sell a Ticket");
                        System.out.println("2. Exit");
                        System.out.println("--------------------------------------------------------------");
                        break;
                    case 3:
                        choice = 3;
                        editBook();
                        break;
                    case 4:
                        choice = 4;
                        deleteBook();
                        break;
                    case 5:
                        choice = 5;
                        addMag();
                        break;
                    case 6:
                        choice = 6;
                        listMag();
                        break;
                    case 7:
                        choice = 7;
                        addDiscMag();
                        break;
                    case 8:
                        choice = 8;
                        listDiscMag();
                        break;    
                    case 9:
                        choice = 9;
                        done = true;
                        break;
                    default:
                        System.out.println("Wrong entry try again.");
                }
            }
            catch(Exception e){
                System.err.println("Error! Try Again!");
            }
        }
    }
    public void addBook(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Author");
        String author = input.nextLine();
        System.out.println("Enter Title");
        String title = input.nextLine();
        System.out.println("How many copies?");
        int copies = input.nextInt();
        System.out.println("Enter Price");
        Double price = input.nextDouble();
        
        Book temp = new Book(author, title, copies, price);
        bookList.add(temp);
    }
    
    public void listBooks(){
        int count = 1;
        for (Publication Book : bookList){
            System.out.println(count + "." + Book);
            count++;
        }
    }
    
    public void deleteBook(){
        Scanner input = new Scanner(System.in);
        listBooks();
        System.out.println("What book do you want to delete?");
        int index = input.nextInt();
        bookList.remove(index - 1);
    }
    
    public void editBook(){
        Scanner input = new Scanner(System.in);
        listBooks();
        System.out.println("What book do you want to edit?");
        int index = input.nextInt();
        System.out.println("What would you like to retitle it?");
        String title = input.nextLine();
        bookList.get(index - 1).setTitle(title);
    }
    
    public void addMag(){
        Scanner input = new Scanner(System.in);
        System.out.println("How many copies do you want to order?");
        int orderQty = input.nextInt();
        System.out.println("For which issue date?");
        String curIssue = input.nextLine();
        Magazine mag2 = new Magazine(orderQty,curIssue);
        magList.add(mag2);

    }
   
    public void listMag(){
       int count = 1;
       for (Publication Magazine : magList){
           System.out.println(count + "." + Magazine);
           count++;
       }
       
    }
    
    public void addDiscMag(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Author");
        String author = input.nextLine();
        System.out.println("Enter Title");
        String title = input.nextLine();
        System.out.println("How many copies do you want to order?");
        int orderQty = input.nextInt();
        DiscMag dMag2 = new DiscMag(author, title,orderQty);
        dMagList.add(dMag2);
    }
    
    public void listDiscMag(){
     int count = 1;
        for (Magazine DiscMag : dMagList){
            System.out.println(count + "." + DiscMag);
            count++;
        }
    }
    
    
}
        